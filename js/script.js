class Employee {
    constructor(name, age, salary) {
      this._name = name;
      this._age = age;
      this._salary = salary;
    }

    get name() {
      return this._name;
    }
  
    get age() {
      return this._age;
    }
  
    get salary() {
      return this._salary;
    }
  
    set name(newName) {
      this._name = newName;
    }
  
    set age(newAge) {
      this._age = newAge;
    }
  
    set salary(newSalary) {
      this._salary = newSalary;
    }
  }
  
  class Programmer extends Employee {
    constructor(name, age, salary, lang) {
      super(name, age, salary);
      this._lang = lang;
    }
  
    get salary() {
      return this._salary * 3;
    }
  
    get lang() {
      return this._lang;
    }
  }
  
  const programmer1 = new Programmer('John Doe', 30, 50000, ['JavaScript', 'Python']);
  const programmer2 = new Programmer('Jane Smith', 25, 60000, ['Java', 'C++']);
  
  console.log('Programmer 1:', programmer1.name, programmer1.age, programmer1.salary, programmer1.lang);
  console.log('Programmer 2:', programmer2.name, programmer2.age, programmer2.salary, programmer2.lang);
  